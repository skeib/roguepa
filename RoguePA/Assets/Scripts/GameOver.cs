﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameOver : MonoBehaviour {

    public Canvas exitMenu;
    public Button gameOverText;
    public Button exitText;

	void Start () {
        exitMenu = exitMenu.GetComponent<Canvas>();
        gameOverText = gameOverText.GetComponent<Button>();
        exitText = exitText.GetComponent<Button>();
        exitMenu.enabled = false;
	}
	
    public void ExitPress()
    {
        exitMenu.enabled = true;
        gameOverText.enabled = false;
        exitText.enabled = false;
    }
	
    public void NoPress()
    {
        exitMenu.enabled = false;
        gameOverText.enabled = true;
        exitText.enabled = true;
    }
    public  void StartLevel()
    {
        Application.LoadLevel(1);
    }
    public void ExitGame()
    {
        Application.Quit();
    }
}
