﻿using UnityEngine;
using System.Collections;

public class Wall : MonoBehaviour {
    public int hitPoints = 2;
    public Sprite damagedWallSprite;

    private SpriteRenderer spriteRenderer;

    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = damagedWallSprite;
    }

    public void DamageWall(int damageRecieved)
    {
        hitPoints -= damageRecieved;
        if (hitPoints <= 0)
        {
            gameObject.SetActive(false);
        }
    }


}
